#!/usr/bin/python3
# encoding: utf-8


import os
import sys
from shutil import copyfile
from jinja2 import Environment, FileSystemLoader
from models import ubuntu, debian


# class RedHatHandler (Base):
#    package_manager = "rpm"
#    package_solver = "yum"


def main(env, self):
    template = env.get_template("docker.j2")
    try:
        if not os.path.exists(f"{self.cwd}/images/{self.name}"):
            os.mkdir(f"{self.cwd}/images/{self.name}")
        self.clone_file()
    except FileNotFoundError as err:
        print(f"{err}")
        sys.exit(1)
    rt = template.render(
        {
            "docker_src": self.docker_src,
            "env": self.create_env(),
            "package_solver": self.package_solver,
            "packages": self.get_packages(),
            "systemd": self.manage_systemd(),
            "files": self.copy_files(),
            "clear": self.clear(),
            "cmds": self.exec_cmds(),
        }
    )
    try:
        if not os.path.exists(f"{self.cwd}/images/{self.name}"):
            os.mkdir(f"{self.cwd}/images/{self.name}")
        with open(f"{self.cwd}/images/{self.name}/Dockerfile", "w") as file:
            file.write(rt)
    except OSError:
        print(f"Creation of the directory {self.cwd}/images/{self.name} failed")


if __name__ == "__main__":
    cwd = os.getcwd()
    try:
        if not os.path.exists(f"{cwd}/images/"):
            os.mkdir(f"{cwd}/images/")
    except OSError:
        print(f"Creation of the directory {cwd}/images/failed")
    images = list()
    images.append(debian.Buster("Debian10", "debian:buster"))
    images.append(ubuntu.Focal("Ubuntu2004", "ubuntu:focal"))
    env = Environment(
        trim_blocks=True, lstrip_blocks=True, loader=FileSystemLoader(cwd)
    )
    for image in images:
        main(env, image)
