#!/usr/bin/python3
#encoding: utf-8

from .debianHandler import DebianHandler

class Buster(DebianHandler):
    def install_de(self):
        return ['xfce4', 'xfce4-goodies']

    def install_system_tools(self):
        tools = super().install_system_tools()
        tools.extend(['qt-at-spi'])
        return tools
