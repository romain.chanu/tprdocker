#!/usr/bin/python3
#encoding: utf-8

from .base import Base

class DebianHandler(Base):
    package_manager = "dpkg"
    package_solver = "apt-get"

    def create_env(self):
        return "ENV DEBIAN_FRONTEND noninteractive"
    def clear(self):
        return "apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*"
