#!/usr/bin/python3
#encoding: utf-8

from .debianHandler import DebianHandler

class Focal(DebianHandler):


    def install_de(self):
        return ['xubuntu-desktop^']

    def install_system_tools(self):
        tools = super().install_system_tools()
        tools.extend(['libgl1-mesa-glx','at-spi2-core'])
        return tools

    def copy_files(self):
        files = super().copy_files()
        files.update(
                {
                    'org.freedesktop.login1.policy':'/usr/share/polkit-1/actions/org.xfce.session.policy',
                    #'org.xfce.session.policy':'/usr/share/polkit-1/actions/org.xfce.session.policy',
                    #mandatory: logins timeout without these options
                    'systemd-logind.override.conf':'/etc/systemd/system/systemd-logind.service.d/override.conf',
                    #optional: change greeter background
                    'lightdm-gtk-greeter.conf':'/etc/lightdm/lightdm-gtk-greeter.conf',
                    #mandatory: remove authenticate popup
                    'color.pkla':'/etc/polkit-1/localauthority/50-local.d/color.pkla'
                    #'color-manager.rules':'/etc/polkit-1/rules.d/color-manager.rules', this version didnt work
                })
        return files

    def manage_systemd(self):
        systems = super().manage_systemd()
        systems.extend([
                "disable blueman-mechanism",
                "disable snapd.seeded.service",
                ])
        return systems

    def exec_cmds(self):
        tools = super().exec_cmds()
        tools.extend([
            #'systemctl enable systemd-logind.service',
            #'apt-get update && apt-get install -y libpam-kwallet5 libkf5wallet-dev libpam-kwallet4'
            'apt-get update && apt-get install -y alsa-utils'
            ])
        return tools
