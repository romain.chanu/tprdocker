#!/usr/bin/python3
# encoding: utf-8

import os
import hashlib
import sys
from shutil import copyfile


class Base:
    package_manager = ""
    package_solver = ""
    cwd = os.getcwd()

    def __init__(self, name, docker_src):
        self.name = name
        self.docker_src = docker_src
        self.packages = list()
        self.systemd = list()

    def create_env(self):
        pass

    def install_x11(self):
        return [
            "dbus-x11",
            "xdg-utils",
            "x11-xserver-utils",
            "x11-utils",
            "libxv1",
            "mesa-utils",
            "mesa-utils-extra",
        ]

    def install_de(self):
        pass

    def install_system_tools(self):
        return [
            "systemd",
            "systemd-sysv",
            "locales",
            "sudo",
            "lightdm",
            "lightdm-gtk-greeter",
            "apt-utils",
            "procps",
            "psmisc",
            "iputils-ping",
            "dialog",
            "net-tools",
        ]

    def install_base_tools(self):
        return ["wget", "curl", "vim", "minicom"]

    def md5_check(self, filename):
        with open(filename, "rb") as to_check:
            data = to_check.read()
            return hashlib.md5(data).hexdigest()

    def get_packages(self):
        self.packages += self.install_x11()
        self.packages += self.install_de()
        self.packages += self.install_system_tools()
        self.packages += self.install_base_tools()
        return self.packages

    def exec_cmds(self):
        return [
            "adduser -u 1000 --disabled-password --gecos '' tpr",
            'echo "tpr    ALL=(ALL)    NOPASSWD: ALL" > /etc/sudoers.d/tpr',
            "sh -c 'echo \"tpr:tpr\" | chpasswd'",
            "usermod -aG dialout tpr",
            # add somewhere else
            #'apt-get update && apt-get install -y syslog-ng'
            "apt-get update && apt-get install -y rsyslog",
            "systemctl disable upower.service",
        ]

    def copy_files(self):
        return {
            "lightdm.conf": "/etc/lightdm/lightdm.conf",
            "PacketTracer_800_amd64_build212_final.deb": "/tmp/PacketTracer_800_amd64_build212_final.deb",
        }

    def manage_systemd(self):
        return [
            #'disable NetworkManager.service',
            #'disable networkd-dispatcher.service',
            #'disable NetworkManager-wait-online.service',
            #'disable network-manager.service',
            "disable systemd-tmpfiles-setup.service"
        ]

    def clone_file(self):
        for file in self.copy_files():
            if os.path.exists(f"{self.cwd}/files/{file}"):
                if not os.path.exists(
                    f"{self.cwd}/images/{self.name}/{file}"
                ) or self.md5_check(
                    f"{self.cwd}/images/{self.name}/{file}"
                ) != self.md5_check(
                    f"{self.cwd}/files/{file}"
                ):
                    copyfile(
                        f"{self.cwd}/files/{file}",
                        f"{self.cwd}/images/{self.name}/{file}",
                    )
            else:
                raise FileNotFoundError(f"File {self.cwd}/files/{file} does not exists")
